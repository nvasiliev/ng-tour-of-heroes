# Install
```
git clone https://github.com/angular/quickstart.git app-name
cd app-name
npm install
```

# Delete non-essential files (optional)
```
xargs rm -rf < non-essential-files.osx.txt
rm src/app/*.spec*.ts
rm non-essential-files.osx.txt
```

If you will init new GIT repo add this `.gitignore` file:
```
.idea/
node_modules/
jspm_packages
npm-debug.log
debug.log
src/**/*.js
!src/systemjs.config.extras.js
!src/systemjs.config.js
!src/systemjs-angular-loader.js
*.js.map
e2e/**/*.js
e2e/**/*.js.map
_test-output
_temp
```

# Development

```
npm start
```
