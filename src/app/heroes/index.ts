export { HeroesComponent } from './heroes.component';
export * from './shared';
export * from './hero-detail';
export * from './hero-search';
