import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';

// To use with the route parameters Observable.
import 'rxjs/add/operator/switchMap';

import { HeroService, Hero } from '../shared';

@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: [ './hero-detail.component.css' ]
})
export class HeroDetailComponent implements OnInit {
  hero: Hero;

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    // Using the paramMap Observable to extract the 'id' parameter value from the ActivatedRoute service.
    // The switchMap operator maps the 'id' in the Observable route parameters to a new Observable,
    // the result of the HeroService.getHero() method.
    this.route.paramMap
      .switchMap((params: ParamMap) => this.heroService.getHero(+params.get('id')))
      .subscribe(hero => this.hero = hero);
  }

  // Going back too far could take users out of the app.
  // In a real app, you can prevent this issue with the CanDeactivate guard.
  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.heroService.update(this.hero)
      .then(() => this.goBack());
  }
}
