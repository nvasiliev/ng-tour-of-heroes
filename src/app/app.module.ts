import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { HttpModule } from '@angular/http';

import { AppComponent }  from './app.component';

import { HeroesComponent, HeroDetailComponent, HeroService, HeroSearchComponent } from './heroes';
import { DashBoardComponent } from './dashboard';

import { AppRoutingModule } from './app-routing.module';

// Imports for loading & configuring the in-memory web api.
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './in-memory-data.service';

@NgModule({
  // Other modules whose exported classes are needed by component templates declared in this module.
  imports: [
    BrowserModule,
    FormsModule, // [(ngModel)] lives here
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule
  ],
  // The view classes that belong to this module.
  // Angular has three kinds of view classes: components, directives, and pipes.
  declarations: [
    AppComponent,
    DashBoardComponent,
    HeroDetailComponent,
    HeroesComponent,
    HeroSearchComponent
  ],
  providers: [ HeroService ], // Injects HeroService to constructor of components, like Singleton.
  bootstrap: [ AppComponent ]
})
export class AppModule { }
