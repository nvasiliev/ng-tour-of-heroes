import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashBoardComponent } from './dashboard';
import { HeroesComponent, HeroDetailComponent } from './heroes';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashBoardComponent },
  { path: 'heroes', component: HeroesComponent },
  { path: 'detail/:id', component: HeroDetailComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],

  // Need for the components in the companion module (AppModule) have access to Router declarables,
  // such as RouterLink and RouterOutlet.
  // https://angular.io/guide/ngmodule-faq#what-should-i-export
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
